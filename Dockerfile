FROM alpine

RUN apk add --update ruby nodejs ruby-rdoc ruby-irb rsync && gem install sass && npm i -g grunt  

WORKDIR /data

# Define default command.
CMD npm install; grunt