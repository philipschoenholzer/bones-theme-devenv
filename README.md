# DevEnv for the Bones Wordpress Theme

## Setup

- Wordpress
- Bones Theme
- Docker
- Grunt


## Installation

1. Clone repository `git clone https://philipschoenholzer@bitbucket.org/philipschoenholzer/bones-theme-devenv.git`
1. Initialize submodules `git submodule init && git submodule update`
1. Install Docker <http://docker.com>
1. Run Docker `docker-compose run --rm grunt`
1. Make any change to a `scss`, `php` or `js` file in the source folder for grunt to be executed (theme is only visible after first change)
1. Open Wordpress installation in the browser `http://localhost:8000/` 
1. Activate the theme "Bones"
1. Finished


## Deployment of the theme to the production server

1. Copy the content of the `theme`-folder to the folder `wp-content/themes/Bones` of the production server 
1. Activate the theme "Bones"

## Possible improvments

### Sync to the cloud

<https://visible.vc/engineering/optimize-wordpress-theme-assets-and-deploy-to-s3-cloudfront/>

### Use Gulp

<https://github.com/synapticism/wordpress-gulp-starter-kit>