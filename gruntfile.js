// Source: https://gist.github.com/jmacqueen/7253789
// Possible improvments for the gruntfile: https://github.com/luetkemj/grunt-for-bones/blob/master/Gruntfile.js
// Blogpost about bones and grunt: http://mattbanks.me/grunt-wordpress-development-deployments/

module.exports = function(grunt) {
	require("matchdep").filterDev("grunt-*", './package.json').forEach(grunt.loadNpmTasks);

	var globalConfig = {
		src: 'src',
		dest: 'theme'
	};

	grunt.initConfig({
		globalConfig: globalConfig,
		pkg: grunt.file.readJSON('package.json'),

		watch: {
			css: {
				files: ['<%= globalConfig.src %>/library/scss/**/*.scss'],
				tasks: ['buildcss']
			},
			php: {
				files: ['<%= globalConfig.src %>/**/*.php'],
				tasks: ['buildphp']
			},
			js: {
				files: ['<%= globalConfig.src %>/**/*.php'],
				tasks: ['buildjs']
			},
			images: {
				files: ['<%= globalConfig.src %>/**/*.jpg','<%= globalConfig.src %>/**/*.png','<%= globalConfig.src %>/**/*.gif'],
				tasks: ['buildimages']
			}
		},

		sass: {
			build: {
				options: {
					trace: true,
					style: 'compressed'
				},
				files: [{
					expand: true,
					cwd: '<%= globalConfig.src %>/library/scss/',
					src: ['*.scss','!_*'],
					dest: '<%= globalConfig.src %>/library/css/',
					ext: '.css'
				}]
			}
		},

		rsync: {
			options: {
				args: ["--verbose"],
				exclude: [".git*","scss",".DS_Store"],
				recursive: true,
				syncDest: true
			},
			build: {
				options: {
					src: "./<%= globalConfig.src %>/",
					dest: "<%= globalConfig.dest %>"
				}
			}
		}

	}); // End initConfig

	grunt.registerTask('default',['watch']);
	grunt.registerTask('buildcss', ['sass','rsync']);
	grunt.registerTask('buildphp', ['rsync']);
	grunt.registerTask('buildjs', ['rsync']);
	grunt.registerTask('buildimages', ['rsync']);

};